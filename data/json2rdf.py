import json
from rdflib import Graph,  RDF,  Namespace, Literal ,OWL
import hashlib

def generate_blocks(filename, graph):
    blockns = Namespace('https://www.unifiedcompliance.com/orchestra.group/block#')
    graph.bind('block', blockns)

    with open(filename, 'r') as file:
        data = json.load(file)
        for block in data:
            #data_block = block['data'].replace(" ", "_").replace(",","")
            data_block = hashlib.md5(block['data'].encode('utf-8')).hexdigest()
            graph.add((blockns[data_block], RDF.type, blockns['block']))
            graph.add((blockns[data_block], blockns['pos'], Literal(block['pos'])))
            graph.add((blockns[data_block], blockns['text_block'], Literal(block['data'])))
            graph.add((blockns[data_block], blockns['link'], Literal(block['link'])))
            graph.add((blockns[data_block], blockns['technic'], Literal(block['technic'])))
            parent = block['parent']
            if parent:
                parent = hashlib.md5(parent.encode('utf-8')).hexdigest()
                graph.add((blockns[data_block], blockns['parent'], blockns[parent]))
            for child in block['children']:
                #child = child.replace(" ", "_").replace(",","")
                child = hashlib.md5(child.encode('utf-8')).hexdigest()
                graph.add((blockns[data_block], blockns['children'], blockns[child]))
        graph.add((blockns['parent'], RDF.type, OWL['ObjectProperty']))
        graph.add((blockns['children'], RDF.type, OWL['ObjectProperty']))
        graph.add((blockns['data'], RDF.type, OWL['DatatypeProperty']))
        graph.add((blockns['pos'], RDF.type, OWL['DatatypeProperty']))
        graph.add((blockns['link'], RDF.type, OWL['DatatypeProperty']))
        graph.add((blockns['text_block'], RDF.type, OWL['DatatypeProperty']))
        graph.add((blockns['technic'], RDF.type, OWL['DatatypeProperty']))

def generate_assets(filename, graph):
    assets = Namespace('https://www.unifiedcompliance.com/orchestra.group/assets#')
    graph.bind('assets', assets)
    blockns = Namespace('https://www.unifiedcompliance.com/orchestra.group/block#')
    with open(filename, 'r') as file:
        data = json.load(file)
        for asset in data:
            asset_hash = hashlib.md5(asset.encode('utf-8')).hexdigest()
            graph.add((assets[asset_hash], RDF.type, assets['assets']))
            graph.add((assets[asset_hash], assets['related_to_block'], blockns[asset_hash]))
            graph.add((assets[asset_hash], assets['text'], Literal(asset)))
            for dep in data[asset]:
                dep = hashlib.md5(dep.encode('utf-8')).hexdigest()
                graph.add((assets[dep], assets['inductive'], assets[asset_hash]))
                #graph.add((dep, RDF.type, asset.replace(" ","_")))
    file.close()
    graph.add((assets['inductive'], RDF.type, OWL['ObjectProperty']))
    graph.add((assets['related_to_block'], RDF.type, OWL['ObjectProperty']))
    graph.add((assets['text'], RDF.type, OWL['DatatypeProperty']))


def generate_threats(filename, graph):
    threats = Namespace('https://www.unifiedcompliance.com/orchestra.group/threats#')
    graph.bind('threats', threats)
    blockns = Namespace('https://www.unifiedcompliance.com/orchestra.group/block#')
    with open(filename, 'r') as file:
        data = json.load(file)
        for threat in data:
            threat_hash = hashlib.md5(threat.encode('utf-8')).hexdigest()
            graph.add((threats[threat_hash], RDF.type, threats['threats']))
            graph.add((threats[threat_hash], threats['related_to_block'], blockns[threat_hash]))
            graph.add((threats[threat_hash], threats['text'], Literal(threat)))
            for dep in data[threat]:
                dep = hashlib.md5(dep.encode('utf-8')).hexdigest()
                graph.add((threats[dep], threats['inductive'], threats[threat_hash]))
            graph.add((threats['text'], RDF.type, OWL['DatatypeProperty']))
    file.close()

def generate_relation(filename, graph):
    blockns = Namespace('https://www.unifiedcompliance.com/orchestra.group/block#')
    graph.bind('block', blockns)
    with open(filename, 'r') as file:
        data = json.load(file)
        for elem in data:
            elem_hash = hashlib.md5(elem.encode('utf-8')).hexdigest()
            for elem2 in data[elem]:
                elem2 = hashlib.md5(elem2.encode('utf-8')).hexdigest()
                graph.add((blockns[elem2], blockns['case_of'], blockns[elem_hash]))
    graph.add((blockns['case_of'], RDF.type, OWL['ObjectProperty']))
    file.close()

if __name__ == "__main__":
    graph = Graph()
    generate_blocks('block_to_rdf.json', graph)
    generate_assets('asset_dependencies.json', graph)
    generate_threats('threats_dependencies.json', graph)
    generate_relation('relations_objects.json', graph)
    with open('nlp_data_2.ttl', 'w') as f:
        print(graph.serialize(format='n3').decode("utf-8"), file=f)

