from blocks_news import Block, Management_Blocks, find_svo, nlp
import spacy
from nltk.tree import Tree
import benepar
from benepar.spacy_plugin import BeneparComponent
from nltk.tree import ParentedTree
import en_core_web_sm
from queue import Queue
from nltk.stem import WordNetLemmatizer
import nltk
import json

def generate_json(object_ser):
    dep = {}
    for block in object_ser:
        dep[block.data] = [dependency.get_data() for dependency in object_ser[block]]
    return dep


def generate_block_to_json(mangment_block):
    arr = []
    for block in mangment_block:
        children = [child.data for child in block.children]
        dict_temp = {'data': block.data, 'parent': block.get_data_parent(), 'children': children, 'pos': block.pos, 'technic': block.technic, 'link': block.link}
        arr.append(dict_temp)
    return arr


def generate_relation(object_ser, management):
    dict_relation = {}
    for subject, verb, obj in object_ser:
        if subject not in dict_relation:
            dict_relation[subject] = []
        dict_relation[subject].append(obj)
        min_block = management.find_minimum_block(verb +" "+ obj)
        dict_relation[subject].append(min_block.get_data())
    return dict_relation

attacks = {'disclosed', 'stolen','escalation', 'reuse', 'lateral movement', 'abuse','obtain','evasion','hravest','dumping','gain','adversary','compromise'}
if __name__ == "__main__":
    thecnic = "T1078"
    paragraph1 = "Adversaries may obtain and abuse credentials of a default account as a means of gaining Initial Access, Persistence, Privilege Escalation, or Defense Evasion. Default accounts are those that are built-into an OS, such as the Guest or Administrator accounts on Windows systems. Default accounts also include default factory/provider set accounts on other types of systems, software, or devices, including the root user account in AWS and the default service account in Kubernetes"
    link1 = "https://attack.mitre.org/techniques/T1078/001/"
    paragraph2 = "Default accounts are not limited to client machines, rather also include accounts that are preset for equipment such as network devices and computer applications whether they are internal, open source, or commercial. Appliances that come preset with a username and password combination pose a serious threat to organizations that do not change it post installation, as they are easy targets for an adversary. Similarly, adversaries may also utilize publicly disclosed or stolen Private Keys or credential materials to legitimately connect to remote environments via Remote Services."
    paragraph3 = "Adversaries may obtain and abuse credentials of a domain account as a means of gaining Initial Access, Persistence, Privilege Escalation, or Defense Evasion.Domain accounts are those managed by Active Directory Domain Services where access and permissions are configured across systems and services that are part of that domain. Domain accounts can cover users, administrators, and services.Adversaries may compromise domain accounts, some with a high level of privileges, through various means such as OS Credential Dumping or password reuse, allowing access to privileged resources of the domain."
    link2 = "https://attack.mitre.org/techniques/T1078/002/"
    paragraph4 = "Adversaries may obtain and abuse credentials of a local account as a means of gaining Initial Access, Persistence, Privilege Escalation, or Defense Evasion. Local accounts are those configured by an organization for use by users, remote support, services, or for administration on a single system or service.Local Accounts may also be abused to elevate privileges and harvest credentials through OS Credential Dumping. Password reuse may allow the abuse of local accounts across a set of machines on a network for the purposes of Privilege Escalation and Lateral Movement."
    link3 = "https://attack.mitre.org/techniques/T1078/003/"
    paragraph5 = "Adversaries may obtain and abuse credentials of a cloud account as a means of gaining Initial Access, Persistence, Privilege Escalation, or Defense Evasion. Cloud accounts are those created and configured by an organization for use by users, remote support, services, or for administration of resources within a cloud service provider or SaaS application. In some cases, cloud accounts may be federated with traditional identity management system, such as Window Active Directory.Compromised credentials for cloud accounts can be used to harvest sensitive data from online storage accounts and databases. Access to cloud accounts can also be abused to gain Initial Access to a network by abusing a Trusted Relationship. Similar to Domain Accounts, compromise of federated cloud accounts may allow adversaries to more easily move laterally within an environment."
    link4 = "https://attack.mitre.org/techniques/T1078/004/"
    paragraphs = [(paragraph1, link1), (paragraph2, link1), (paragraph3, link2) ,(paragraph4,link3) , (paragraph5, link4)]
    asset_dependencies = {}
    threats_dependencies = {}
    blocks = []
    relation = {}
    with open('conclusion_model', 'w+') as file:
        counts_sentence = 0
        file.write("technics num "+ thecnic + '\n')
        file.write("url: https://attack.mitre.org/techniques/T1078/" +'\n')
        print("keywords are " + str(attacks), file=file)
        for paragraph, link in paragraphs:
            file.write("#############################################################################################\n")
            print("the paragraph :" , file=file)
            print(paragraph, file=file)
            doc = nlp(paragraph)
            sentences = list(doc.sents)
            for i in range(len(sentences)):
                file.write("*"*200+'\n')
                print("sentence number " + str(i) + " :  " + sentences[i].text, file=file)
                sentence = nlp(sentences[i].text)
                tr = Tree.fromstring(list(sentence.sents)[0]._.parse_string)
                newtree = ParentedTree.convert(tr)
                managment = Management_Blocks(newtree, paragraph, link)
                blocks += generate_block_to_json(managment)
                assets, threats = managment.find_assets_and_threats_from_blocks(attacks)
                file.write("\n")
                print("the assets are " + str(assets), file=file)
                file.write("\n")
                if len(threats) == 0:
                    print("the are not found threats " , file=file)
                else:
                    print("the threats are " + str(threats), file=file)
                file.write("\n")
                noun = []
                asset_text = [a.get_data() for a in assets]
                threats_text = [a.get_data() for a in threats]
                asset_text = set(asset_text)
                threats_text = set(threats_text)
                for chunk in sentence.noun_chunks:
                    if chunk.text in asset_text:
                        noun.append("{}/asset".format(chunk.text))
                    elif chunk.text in threats_text:
                        noun.append("{}/threat".format(chunk.text))
                    else:
                        noun.append(chunk.text)
                verb = [token.text for token in sentence if token.pos_ == "VERB"]
                print("Noun phrases:", noun, file=file)
                file.write('\n')
                print("Verbs:", verb, file=file)
                file.write('\n')
                print("subject verb object", file=file)
                noun = [chunk.text for chunk in sentence.noun_chunks]
                print(find_svo(sentence.text, noun, verb, asset_text, threats_text), file=file)
                tmp = generate_relation(find_svo(sentence.text, noun, verb, asset_text, threats_text), managment)
                relation ={**relation, **tmp}
                file.write("\n")
                print("assets relations", file=file)
                print(managment.get_dependencies_assets(), file=file)
                tmp = generate_json(managment.get_dependencies_assets())
                asset_dependencies.update(tmp)
                print("threats relations", file=file)
                print(managment.get_dependencies_threats(), file=file)
                tmp = generate_json(managment.get_dependencies_threats())
                threats_dependencies = {**threats_dependencies, **tmp}
            file.write("\n\ntotal sentences : " + str(len(sentences)) +'\n')
            counts_sentence += len(sentences)
        file.write("\n\n the total sentence the models analyze is " + str(counts_sentence))
        file.close()
        with open('asset_dependencies.json', 'w+') as file:
            json.dump(asset_dependencies, file)
        file.close()
        with open('threats_dependencies.json', 'w+') as file:
            json.dump(threats_dependencies, file)
        file.close()
        with open('block_to_rdf.json', 'w+') as file:
            json.dump(blocks, file)
        file.close()
        with open('relations_objects.json', 'w+') as file:
            json.dump(relation, file)
        file.close()




