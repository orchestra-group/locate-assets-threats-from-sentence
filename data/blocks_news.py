import spacy
from nltk.tree import Tree
import benepar
from benepar.spacy_plugin import BeneparComponent
from nltk.tree import ParentedTree
import en_core_web_sm
from queue import Queue
from nltk.stem import WordNetLemmatizer
import nltk
from enum import Enum
import re
import json

nltk.download('wordnet')
benepar.download('benepar_en3')
if spacy.__version__.startswith('2'):
    nlp = spacy.load('en_core_web_sm')
    nlp.add_pipe(BeneparComponent('benepar_en3'))
else:
    nlp = en_core_web_sm.load()
    nlp.add_pipe("benepar", config={"model": "benepar_en3"})





class Block:
    class Block_classify(Enum):

        UNDIFIEND = 0
        ASSETS = 1
        THREATS = 2
        MITEGATION = 3

    def __init__(self, node, stage, technic, link):
        self.data = " ".join(node.leaves())
        self.origin_subtree = node
        self.children = []
        self.parent = None
        self.next = None
        self.explain = None
        self.stage = stage
        self.type = self._get_type_block(node.label(), stage)
        self.pos = node.label()
        self.state = Block.Block_classify.UNDIFIEND
        self.technic = technic
        self.link = link

    def _get_type_block(self, pos_tag, stage):
        if pos_tag == 'NP' and stage == 0:
            return 'noun1'
        elif pos_tag == 'NP' and stage == 2:
            return 'noun2'
        elif pos_tag == 'VP':
            return 'action'
        elif pos_tag == 'PP':
            return 'link/explanation'
        elif pos_tag == 'S':
            return 'sentence'
        elif pos_tag == 'JJ':
            return 'property'
        elif pos_tag == 'CC':
            if self.data == 'and':
                return 'And condition'
            elif self.data == 'or':
                return 'OR condition'
        elif pos_tag == 'SYN':
            return 'OR condition'
        else:
            return pos_tag

    def set_parent(self, parent):
        self.parent = parent

    def set_state(self, state):
        if self.state == Block.Block_classify.UNDIFIEND:
            self.state = state
        else:
            print("change state from {} to {}".format(str(self.state), str(state)))
            self.state = state

    def add_child(self, child):
        if child:
            self.children.append(child)

    def set_explain(self, block_explain):
        self.explain = block_explain

    def get_data(self):
        return self.data

    def get_data_parent(self):
        if self.parent:
            return self.parent.data
        return self.parent

    def get_type_block(self):
        return self.type

    def set_next(self, block):
        self.next = block

    def __repr__(self):
        return self.data

    def __iter__(self):
        block = self
        while block:
            yield block
            block = block.next


class Management_Blocks:
    stopwords = set(nlp.Defaults.stop_words)
    lemmatizer = WordNetLemmatizer()

    def __init__(self, node, technic, link):
        self.technic = technic
        self.link = link
        self.root = self.build_tree_v1(node)
        self._concrete_block()
        self.set_of_assets = set()
        self.set_of_threats = set()

    def getVerbText(self, node, txt=''):
        dar = None
        for n in node:
            if type(n) is Tree or type(n) is ParentedTree:
                tip = n.label()
                if tip == "NP" or tip == "PP" or tip == "S":
                    dar = n
                    break
                rez = self.getVerbText(n)
                dar = rez[1]
                txt = txt + " " + rez[0]
            else:
                txt = txt + " " + n
        return txt, dar

    def build_tree(self, root, parent=None, stage=0):
        if isinstance(root, Tree):
            root = Block(root, stage)
            root.set_parent(parent)
        for child in root.origin_subtree:

            # arrive to the leaves in the original tree
            if isinstance(child, str):
                return root

            # check if it noun and if it first noun or second noun
            if child.label() == 'NP':
                if stage == 0:
                    stage = 1
                    block = self.build_tree(child, root, 0)
                    root.add_child(block)
                else:
                    stage = 3
                    block = self.build_tree(child, root, 0)
                    root.add_child(block)

            # the tree present a verb
            elif child.label() == 'VP' and stage < 3:
                stage = 2
                rez = self.getVerbText(child)
                if rez[1]:
                    par = rez[1].parent()
                    jau = False
                    for nd in par:
                        if nd.label() == rez[1].label():
                            jau = True  # already
                        if jau:
                            block = self.build_tree(par, root, stage)
                            root.add_child(block)
            # tree present a  explanation of previous tree
            elif child.label() == 'PP':
                block = self.build_tree(child, root, 0)
                root.add_child(block)
            # the sentence is complex sentence
            elif child.label() == 'S':
                block = self.build_tree(child, root, 0)
                root.add_child(block)
            else:
                block = self.build_tree(child, root, 0)
                root.add_child(block)
        return root

    def build_tree_v1(self, root, parent=None):
        if isinstance(root, Tree):
            root = Block(root, 0, self.technic, self.link)
            root.set_parent(parent)
        for child in root.origin_subtree:
            if isinstance(child, str):
                return root
            if len(child.leaves()) == 2:
                child = Tree.fromstring(list(nlp(" ".join(child.leaves())).sents)[0]._.parse_string)
            # if child.label() == 'S': #TODO: try to think how to solve with new sentence
            #     root.add_child(Management_Blocks(Tree.fromstring(list(nlp(" ".join(child.leaves())).sents)[0]._.parse_string)))
            #     return root
            # child = Tree.fromstring(list(nlp(" ".join(child.leaves())).sents)[0]._.parse_string)
            block = self.build_tree_v1(child, root)
            root.add_child(block)
        return root

    def _check_condition_of_threat(self, word, set_of_attacks):
        individuals = set()
        for _ in set_of_attacks:
            for w in _.split():
                individuals.add(w)
        if Management_Blocks.lemmatizer.lemmatize(word.lower()) in set_of_attacks or word.lower() in individuals:
            return True
        return False

    def _check_elem_in_set(self, block, set):
        for elem in set:
            if elem.get_data() == block.get_data():
                return True
        return False

    def _find_threats_blocks(self, set_of_attacks):
        set_threats = set()
        for attack in set_of_attacks:
            block = self.find_minimum_block(attack)
            if block and not block.children:
                block = block.parent
            if block:
                words = block.get_data().split()
                if len(words) == 2 and words[0] in Management_Blocks.stopwords:
                    pass
                else:
                    if not self._check_elem_in_set(block, set_threats):
                        block.set_state(Block.Block_classify.THREATS)
                        set_threats.add(block)
        return set_threats

    def _find_assets_blocks(self, set_of_attacks):
        asset_set = set()
        for current_block in self.root:
            if current_block.pos == 'NP' or current_block.pos == 'NN' or current_block.pos == 'NNP' or current_block.pos == 'NNS':
                if self._check_condition_on_assets(current_block, set_of_attacks):
                    words = current_block.get_data().split()
                    if len(words) == 2 and words[0] in Management_Blocks.stopwords:
                        pass
                    else:
                        if not self._check_elem_in_set(current_block, asset_set):
                            current_block.set_state(Block.Block_classify.ASSETS)
                            asset_set.add(current_block)
        return asset_set

    def _check_condition_on_assets(self, block, set_of_attacks):
        if block.get_data() in set_of_attacks:
            return False
        words = block.get_data().split()
        for word in words:
            if self._check_condition_of_threat(word, set_of_attacks):
                return False
        return True

    def find_assets_and_threats_from_blocks(self, set_of_attacks):
        """
        find assets and threats blocks from KBs
        :return:
        """

        if len(self.set_of_assets) == 0 and len(self.set_of_threats) == 0:
            self.set_of_assets = self._find_assets_blocks(set_of_attacks)
            self.set_of_threats = self._find_threats_blocks(set_of_attacks)
            return self.set_of_assets, self.set_of_threats
        else:
            return self.set_of_assets, set_of_attacks

    def _concrete_block(self):
        queue = Queue()
        queue.put(self.root)
        prev_block = self.root
        while not queue.empty():
            node = queue.get()
            for next_block in node.children:
                # if isinstance(next_block, Management_Blocks):
                #     next_block = next_block.root
                prev_block.set_next(next_block)
                queue.put(next_block)
                prev_block = next_block

    def __iter__(self):
        queue = Queue()
        queue.put(self.root)
        yield self.root
        while not queue.empty():
            node = queue.get()
            for current_block in node.children:
                yield current_block
                queue.put(current_block)

    def lemmatizer_sentence(self, sentence):
        words = sentence.split()
        if len(words) > 1:
            lemmatizer_sentence = list(map(Management_Blocks.lemmatizer.lemmatize, words))
            return " ".join(lemmatizer_sentence)
        return Management_Blocks.lemmatizer.lemmatize(sentence)

    def get_dependencies_assets(self):
        asset_dependencies = {}
        for elem in self.set_of_assets:
            asset_dependencies[elem] = []
            for dependencies in self.set_of_assets:
                if dependencies.get_data() in elem.get_data() and elem != dependencies:
                    asset_dependencies[elem].append(dependencies)
        return asset_dependencies

    def get_dependencies_threats(self):
        asset_dependencies = {}
        for elem in self.set_of_threats:
            asset_dependencies[elem] = []
            for dependencies in self.set_of_threats:
                if dependencies.get_data() in elem.get_data() and elem != dependencies:
                    asset_dependencies[elem].append(dependencies)
        return asset_dependencies

    def _search_block(self, sentence, isreasoning=False):
        minimum_block = None
        for current_block in self:
            if not isreasoning and sentence in current_block.get_data().lower():
                if current_block.parent:
                    if current_block.get_data() != current_block.parent.get_data():
                        minimum_block = current_block
                else:
                    minimum_block = current_block
            elif isreasoning and all(
                    element in self.lemmatizer_sentence(current_block.get_data().lower()) for element in
                    self.lemmatizer_sentence(sentence.lower()).split()):
                minimum_block = current_block
        return minimum_block

    def find_minimum_block(self, sentence):
        minimum_block = self._search_block(sentence)
        if not minimum_block:
            minimum_block = self._search_block(sentence, True)
        return minimum_block


def find_closet_sub(sentence, noun_list, asset_set, threats_set):
    dist_noun = -1
    noun_closet = ''
    for noun in noun_list:
        found = re.search('\\b' + noun + '\\b', " ".join(sentence))
        if found and (noun in asset_set or noun in threats_set):
            partial = noun.split()[0]
            index = sentence.index(partial)
            if index > dist_noun:
                dist_noun = index
                noun_closet = noun
    return noun_closet


def find_closet_obj(sentence, noun_list, asset_set, threats_set):
    dist_noun = len(sentence) + 1
    noun_closet = ''
    for noun in noun_list:
        found = re.search('\\b' + noun + '\\b', " ".join(sentence))
        if found and (noun in asset_set or noun in threats_set):
            partial = noun.split()[0]
            index = sentence.index(partial)
            if index < dist_noun:
                dist_noun = index
                noun_closet = noun
    return noun_closet


def find_svo(sentence, noun_list, verb_list, assets_set, threats_set):
    import string
    sentence = [word if word not in string.punctuation else ' ' for word in sentence]
    words = "".join(sentence).split()
    relations = []
    for verb in verb_list:
        index = words.index(verb)
        sub = find_closet_sub(words[: index], noun_list, assets_set, threats_set)
        obj = find_closet_obj(words[index + 1:], noun_list, assets_set, threats_set)
        relations.append((sub, verb, obj))
    return relations


if __name__ == "__main__":
    tr = Tree.fromstring(list(
        nlp("Similarly, adversaries may also utilize publicly disclosed or stolen Private Keys or credential materials to legitimately connect to remote environments via Remote Services"
            ).sents)[0]._.parse_string)
    newtree = ParentedTree.convert(tr)
    a = Management_Blocks(newtree)
    asset, threats = a.find_assets_and_threats_from_blocks(
        {'escalation', 'reuse', 'lateral movement', 'abuse', 'obtain', 'evasion', 'hravest', 'dumping', 'gain',
         'adversary', 'compromise'})
    print(asset)
    print(threats)

    # found verb and nouns
    doc = nlp(
        "Similarly, adversaries may also utilize publicly disclosed or stolen Private Keys or credential materials to legitimately connect to remote environments via Remote Services.")
    noun = []
    asset_text = [a.get_data() for a in asset]
    threats_text = [a.get_data() for a in threats]
    asset_text = set(asset_text)
    threats_text = set(threats_text)
    for chunk in doc.noun_chunks:
        if chunk.text in asset_text:
            noun.append("{}/asset".format(chunk.text))
        elif chunk.text in threats_text:
            noun.append("{}/threat".format(chunk.text))
        else:
            noun.append(chunk.text)
    print("Noun phrases:", noun)
    print("Verbs:", [token.lemma_ for token in doc if token.pos_ == "VERB"])
    print(a.get_dependencies_assets())
    noun = [chunk.text for chunk in doc.noun_chunks]
    verbs = [token.text for token in doc if token.pos_ == "VERB"]
    print(find_svo(doc.text, noun, verbs, asset_text, threats_text))
